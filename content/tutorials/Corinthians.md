---
title: "Corinthians"
date: 2021-09-18T23:28:40-03:00
draft: false
image: 'https://i.imgur.com/NsfXH7t.png'
---

**Sport Club Corinthians Paulista: Uma História de Raça e Fidelidade 🏆⚫⚪**

 **Origens e Fundação 🌐:**

   O Sport Club Corinthians Paulista, fundado em 1910, surgiu no bairro do Bom Retiro, em São Paulo. Criado por operários, o clube nasceu com uma identidade popular e operária, tornando-se uma verdadeira paixão para milhões de torcedores. O famoso grito "Vai, Corinthians!" ecoa nos estádios e transcende fronteiras.

**Palmarés Imponente 🏅:**

   O Corinthians ostenta um currículo repleto de títulos expressivos, incluindo seis Campeonatos Brasileiros, uma Copa do Brasil, além de notáveis conquistas internacionais, como a Copa Libertadores e o Mundial Interclubes. A Fiel Torcida, como é conhecida a torcida corinthiana, é parte fundamental desses triunfos, empurrando o time nos momentos decisivos.

**Ícones Inesquecíveis 🌟:**

   O clube viu surgir ídolos que se tornaram eternos. A majestosa figura de Sócrates, o artilheiro Ronaldo Fenômeno, e a lealdade de Ralf são apenas alguns exemplos. Cada um contribuiu para a construção da rica história do Corinthians, cimentando sua posição entre os grandes do futebol brasileiro.

 **Curiosidades Marcantes 🤔:**

   O Corinthians é marcado por curiosidades fascinantes, como a Democracia Corinthiana nos anos 80, um movimento que uniu jogadores e torcedores em decisões importantes para o clube. Além disso, a construção da Arena Corinthians para a Copa do Mundo de 2014 é um marco arquitetônico e esportivo.

 **Elenco Atual e Futuro 🔄:**

   O Corinthians continua a tradição de formar equipes competitivas. Com um elenco que mescla juventude e experiência, o clube busca manter-se no topo do futebol brasileiro. A paixão da torcida corinthiana é o combustível que impulsiona o time a superar desafios e a perseguir novos títulos. ⚽🖤⚪️
  

